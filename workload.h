#pragma once
#include <optional>
#include <mutex>

namespace workloads{

	class Workload {
	public:
		Workload();
		virtual ~Workload();
		//std::optional<float> load(const float start);
		std::optional<float> load(const float low, const float high, const int iterations, const float start);

	private:

		float square(float x);
		float derivative(float x);

		std::mutex g_display_mutex;
	};

}
