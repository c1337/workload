#include <iostream>
#include <thread>
#include <cmath>
#include <chrono>
#include <exception>
#include "workload.h"


namespace workloads{

	Workload::Workload() {

	}

	Workload::~Workload() {

	}


	std::optional<float> Workload::load(const float low, const float high, const int iterations, const float start){

		float x = start;
		float x_new = 0;
		float y = square(start);
		float y_old = y+1;
		float gradient = 0;
		float leap = abs(high-start) > abs(low-start) ? abs(high-start) : abs(low-start);

		try{
			for(int i = 1; i <= iterations; i++){

				gradient = derivative(x);
				leap = gradient > 0 ? -leap/i : leap/i ; // TODO: logarithmic decaying leap
				x_new = x+leap;
				y = square(x_new);
				if (y < y_old){
					y_old = square(x_new);
					x = x_new;
				}
			}

		    std::thread::id this_id = std::this_thread::get_id();
		    g_display_mutex.lock();
		    std::cout << this_id << "\t" << low << " " << high << " " << start << " " << x << " " << y << std::endl;
		    g_display_mutex.unlock();
		} catch (std::exception& ex)
		{
			std::cout << "Exception occurred:\t" << ex.what() << std::endl;
			return std::nullopt;
		}
	    return y;
	}

	float Workload::square(float x){
		return std::pow(x,2);
	}

	float Workload::derivative(float x){
		return x*2;
	}
}

